package ru.t1.artamonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.request.ProjectRemoveByIndexRequest;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-remove-by-index";

    @NotNull
    private static final String DESCRIPTION = "Remove project by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpointClient().removeByIndex(request);
    }

}
