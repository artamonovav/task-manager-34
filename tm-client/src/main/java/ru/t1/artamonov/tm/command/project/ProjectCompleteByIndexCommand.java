package ru.t1.artamonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-complete-by-index";

    @NotNull
    private static final String DESCRIPTION = "Complete project by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(Status.COMPLETED);
        getProjectEndpointClient().changeProjectStatusByIndex(request);
    }

}
