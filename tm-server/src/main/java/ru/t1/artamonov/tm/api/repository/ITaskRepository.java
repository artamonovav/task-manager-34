package ru.t1.artamonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>, IUserOwnedRepository<Task> {

    @Nullable
    Task create(@Nullable String userId, @Nullable String name);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

}
